json.extract! controller, :id, :post, :created_at, :updated_at
json.url controller_url(controller, format: :json)
